<?php
use Symfony\Component\HttpFoundation\Request;

require_once __DIR__.'/../vendor/autoload.php';

$app = new Silex\Application();

//mounting route with ControllerProvider
$app->mount('/', new App\ControllerProvider());

//setting database connection
$app->register(new \Silex\Provider\DoctrineServiceProvider(), array(
    'db.options' => array(
        'dbname' => 'assessment_schema',
        'user' => 'gilbert',
        'password' => 'gilbert90',
        'host' => '127.0.0.1',
        'driver' => 'pdo_mysql',
    ),
));


$app->before(function (Request $request) use ($app){
    if (strpos($request->headers->get('Content-Type'), 'application/json') === 0) {
        $data = json_decode($request->getContent(), true);
        $request->request->replace(is_array($data) ? $data : array());
    }
});

$app->run();



