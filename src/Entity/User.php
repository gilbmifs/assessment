<?php

namespace RizkBundle\Entity;

class User {
    private $id;
    private $username;
    private $fullname;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set id
     *
     * @return integer
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set username
     *
     * @return username
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getFullname()
    {
        return $this->fullname;
    }

    /**
     * Set username
     *
     * @return username
     */
    public function setFullName($fullname)
    {
        $this->fullname = $fullname;

        return $this;
    }

    public function __construct()
    {
    }



    public function addUser(User $user, $app){
        $app['db']->insert('user', array(
                'fullname' => 'Gabriel',
                'username' => 'Gagno'
            )
        );
        return;
    }

}