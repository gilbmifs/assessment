<?php

namespace App;

use Symfony\Component\HttpFoundation\Request;
use Silex\Api\ControllerProviderInterface;
use Silex\Application as App;

class ControllerProvider implements ControllerProviderInterface
{


    public function connect(App $app)
    {
        $controllers = $app['controllers_factory'];

        //Add items
        $controllers->post('/additem', 'App\ControllerProvider::postAdditemAction');

        //Add items to user
        $controllers->post('/additemtouser', 'App\ControllerProvider::postAddItemtoUserAction');

        //Remove item from user
        $controllers->delete('/removeitemfromuser/{user_id}/{item_id}', 'App\ControllerProvider::deleteRemoveitemAction');

        //Exchange items to another item
        $controllers->patch('/exchangeitem/{user_id}/{itemtoreplace}/{replacementitem}', 'App\ControllerProvider::patchExchangeItemAction');


        return $controllers;
    }

    /**
     * @param App $app
     * @param Request $request
     * @return string
     */
    function postAdditemAction(App $app, Request $request)
    {
        //used to add an item in the available items
        $type = array( 'type' => $request->request->get('type'));
        $post = $app['db']->insert('items', $type);
        return ($post == 1) ? "Successful" : "Failed";
    }

    /**
     * @param App $app
     * @param Request $request
     * @return string
     */
    function postAddItemtoUserAction(App $app, Request $request)
    {
        //get data from request
        $user_id = $request->request->get('userid');
        $item_id = $request->request->get('itemid');

        //check if item for user already exists
        $data = $app['db']->fetchAssoc("select * from user_items where user_id = $user_id and item_id = $item_id");


        if($data !== false){
            //if exists update quantity
            $sql = "UPDATE user_items set quantity = quantity+1 where id = ".$data['id'];
            $post = $app['db']->executeQuery($sql);
        }else{
            //if doesn't exist add item with quantity 1
            $data = array( 'user_id' => $request->request->get('userid'), 'item_id' => $request->request->get('itemid'), 'quantity' => '1' );
            $post = $app['db']->insert('user_items', $data);
        }

        return ($post == 1) ? "Successful" : "Failed";
    }

    /**
     * @param App $app
     * @param user_id
     * @param item_id
     * @return string
     */
    function deleteRemoveitemAction(App $app, $user_id, $item_id){

        //get item from table
        $data = $app['db']->fetchAssoc("select * from user_items where user_id = $user_id and item_id = $item_id");

        //if not found stop execution and display message
        if($data === false)
            return 'Item not found';

        if($data['quantity'] == 1){
            //if quantity is 1 delete record
            $delete = $app['db']->delete('user_items', array( 'user_id' => $user_id, 'item_id' => $item_id));
        }else{
            //if quantity is more than 1 reduce 1 from quantity
            $sql = "UPDATE user_items set quantity = quantity-1 where id = ".$data['id'];
            $delete = $app['db']->executeQuery($sql);
        }

        return ($delete == 1) ? "Successful" : "Failed";
    }

    /**
     * @param App $app
     * @param user_id
     * @param itemtoreplace
     * @param replacementitem
     * @return string
     */
    function patchExchangeItemAction(App $app, $user_id, $itemtoreplace, $replacementitem){

        //check if item to replace is available for user
        $data = $app['db']->fetchAssoc("select * from user_items where user_id = $user_id and item_id = $itemtoreplace");

        if($data === false)
            return 'Item not found';

        if($data['quantity'] == 1){
            try{
                // if quantity available is 1 execute this section
                $app['db']->beginTransaction();
                $app['db']->delete('user_items', array( 'user_id' => $user_id, 'item_id' => $itemtoreplace));

                //get data for item to replace with
                $data = $app['db']->fetchAssoc("select * from user_items where user_id = $user_id and item_id = $replacementitem");

                if($data === false){
                    //if item doesn't exist create record
                    $data = array( 'user_id' => $user_id, 'item_id' => $replacementitem, 'quantity' => '1' );
                    $app['db']->insert('user_items', $data);
                }else{
                    //if item exists add quantity
                    $sql = "UPDATE user_items set quantity = quantity+1 where id = ".$data['id'];
                    $app['db']->executeQuery($sql);
                }

                $app['db']->commit();
                return 'Successful';
            }catch (\Exception $e) {
                $app['db']->rollBack(); 
                return 'Failed';
            }
            
        }else{
            try{
                $app['db']->beginTransaction();
                //reduce one from quantity if quantity is more than 1
                $sql = "UPDATE user_items set quantity = quantity-1 where id = ".$data['id'];
                $app['db']->executeQuery($sql);

                //get data for item that is going to be replaced with
                $data = $app['db']->fetchAssoc("select * from user_items where user_id = $user_id and item_id = $replacementitem");
                if($data === false){
                    // if item doesn't exist for user add record with quantity 1
                    $data = array( 'user_id' => $user_id, 'item_id' => $replacementitem, 'quantity' => '1' );
                    $app['db']->insert('user_items', $data);
                }else{
                    //if exists already add quantity by 1
                    $sql = "UPDATE user_items set quantity = quantity+1 where id = ".$data['id'];
                    $app['db']->executeQuery($sql);
                }
                $app['db']->commit();
                return 'Successful';
            }catch (\Exception $e) {
                $app['db']->rollBack(); 
                return 'Failed';
            }
        }

    }

}
